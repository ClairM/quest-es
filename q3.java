/*Para executar:
	No prompt de comando dentro do diretorio do arquivo execute "javac q3.java" para compilar
	Depois use "java q3" para executar o codigo
	Ou vc pode executar e compilar diretamente no eclipse
*/
public class q3 {

	public static void main(String[] args){

		double[] ts = {7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7, 1.7};
		System.out.print(closesToZero(ts));
	}

	public static double closesToZero(double[] ts){
		double temp = ts[0];
		for(int i = 1; i < ts.length; i++){
			// verifica se tem algum valor 0 e o retorna
			if(ts[i] == 0 || temp == 0){
				return 0;
				//verifica se são absolutamente iguais e depois pega o positivo 
			} else if(Math.abs(temp) == Math.abs(ts[i])){
				if(temp < 0){
					temp = ts[i];
				} 
				// pega o menor numero absoluto
			} else if(Math.abs(temp) > Math.abs(ts[i])){
				temp = ts[i];
			}
		}

		return temp;
	}

}