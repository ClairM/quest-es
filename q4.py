import urllib.request #resquest url
import re #expressões regulares
import sys #utilizado para configurar impressão no console
import codecs #utilizado para configurar impressão no console

url = 'http://www.submarino.com.br'

#necessario para impressao do caracteres no console do windows
if sys.stdout.encoding != 'cp850':
  sys.stdout = codecs.getwriter('cp850')(sys.stdout.buffer, 'strict')
if sys.stderr.encoding != 'cp850':
  sys.stderr = codecs.getwriter('cp850')(sys.stderr.buffer, 'strict')

#Fazer o download do html do submarinoo colocando informações no header da requisição
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
response = opener.open(url)
html = response.read()
#tira erros de formatacao para não prejudicar na busca  
html = str(html, encoding='utf-8', errors = 'ignore')

#pega uma area do html onde se encontra o produto mais vendido
content = re.search(r'mais-vendidos(.+)submit-area', html, re.S).group()

#Pega a descrição do produto
subcontent = re.search(r'title="(.+?)"', content, re.S)
produto = subcontent.group(1)
print("*****Info Produto*****")
print(produto + "\n")

#Pega o preço do produto
priceInteger = re.search(r'product-price-integer">(.+?)<', content, re.S)
priceDecimal = re.search(r'product-price-decimal">(.+?)<', content, re.S)
price = priceInteger.group(1) + priceDecimal.group(1)
print("*****Preço do Produto*****")
print(price)
